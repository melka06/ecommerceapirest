var models = require("../database/models");

module.exports = {
    
    // getAll
    getAll: function(req, res) {        
        models.Categorie.findAll().then(categories => {
            return (categories == null) ? false : categories;
        });
    },

    // getOne id
    getCategorie: function (req, res) {        
        var id = parseInt(req.params.id);

        // on récupere la catégorie
        models.Categorie.findById(id).then(categorie => {
            if(categorie!=null){
                // la categorie existe, on renvoi la categorie.
                var messageRetour = {
                    status: "success",
                    datas : categorie
                }
                res.json(messageRetour);
            }else{
                // la categorie n'existe pas.
                res.json({status: "error", message : "La categorie n'existe pas."});
            }
        });
    },

    // update id 
    // token require + admin 1
    update: function (req, res) {         
        var id = parseInt(id);

        // on vérifie si la categorie à modifier existe 
        models.Categorie.findById(id).then(categorie => {
            if (categorie == null){
                // La categorie n'existe pas.
                res.json({status: "error", message : "La categorie que vous souhaitez modifier n'existe pas"});
            }else{
                // on update l'entité
                models.Categorie.update(req.body, {
                    where: {
                        id: id
                    }
                });

                res.json({status: "success", message : "La catégorie n° " + id + " a été modifié."});
            }
        });
    },

    // create 
    // token require + admin 1
    create: function (req, res) {        
        // on vérifie que l'utilisateur logé est admin
        if(req.authInfo.admin != 1)
            // l'user n'existe pas.
            res.json({status: "error", message : "L'user n'existe pas."});

        // On récupère les informations de la catégories depuis les paramètres POST.
        var newCategorie = models.Categorie.build({
            nom:            req.body.nom,
            description:    req.body.description
        });

        // On réalise une transaction vers la db.
        models.sequelize.transaction(function(t){
            // Si l'entité n'existe pas on l'ajoute.
            return models.Categorie.findOrCreate({
                where: {
                    nom:            newCategorie.nom,
                    description:    newCategorie.description
                },
                transaction: t
            })
            .spread(function(categorieResultat, created){ 
                // true si l'entité a été créé. False si elle existe deja
                if (created){
                    // L'entité à été ajouté.
                    res.json({status: "success", message : "Categorie ajouté."});
                }
                else {
                    // L'entité existe deja dans la db.
                    res.json({status: "error", message : "La catégorie existe déjà."});
                }     
            }); 
        });
    },

    // daelete
    // token require + admin 1
    delete: function (req, res) {   
        // on vérifie que l'utilisateur logé est admin
        if(req.authInfo.admin != 1)
            // l'user n'existe pas.
            res.json({status: "error", message : "L'user n'existe pas."});

        var id = parseInt(req.params.id);

        // on vérifie si la categorie à supprimer existe 
        models.Categorie.findById(id).then(categorie => {
            if (categorie == null){
                // La categorie n'existe pas.
                res.json({status: "error", message : "La categorie que vous souhaitez supprimer n'existe pas"});
            }else{
                // on supprime l'entité
                categorie.destroy();

                res.json({status: "success", message : "La catégorie n° " + id + " a été supprimé."});
            }
        });      
    }   
}