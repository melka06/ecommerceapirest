var models = require("../database/models");

module.exports = {
    
    // getAll
    getAll: function (req, res) {        
        models.Produit.findAll().then(produits => {
            if (produits == null || produits.length == 0){
                // Aucun produit dans la db.
                res.json({status: "error", message : "Aucun produit."});
            }else{
                // On renvoi les produits
                res.json({status: "success", data : produits});                
            }
        });
    },

    // getOne id
    getProduit:  function (req, res) {        
        var id = parseInt(req.params.id);

        // on récupere le produit
        models.Produit.findById(id).then(produit => {
            if(produit != null){
                // le produit existe, on renvoi le.
                res.render("produit", {error: false, produit});
            }else{
                // le produit n'existe pas.
                res.render("produit", {error: true, message : "Le produit n'existe pas."});
            }
        });
     },

    // update id
    // token require + admin 1
    update:  function (req, res) {        
        // on vérifie que l'utilisateur logé est admin
        if(req.authInfo.admin != 1)
            // l'user n'existe pas.
            res.json({status: "error", message : "L'user n'existe pas."});

        var id = parseInt(req.params.id);

        // on vérifie si le produit à modifier existe 
        models.Produit.findById(id).then(produit => {
            if (produit == null){
                // Le produit n'existe pas.
                res.json({status: "error", message : "Le produit que vous souhaitez modifier n'existe pas"});
            }else{
                // on update l'entité
                models.Produit.update(req.body, {
                    where: {
                        id: id
                    }
                });

                res.json({status: "success", message : "Le produit n° " + id + " a été modifié."});
            }
        }); 
           
     },

    // create
    // token require + admin 1 
    create:  function (req, res) {        
        // on vérifie que l'utilisateur logé est admin
        if(req.authInfo.admin != 1)
            // l'user n'existe pas.
            res.json({status: "error", message : "L'user n'existe pas."});

        // On recupere les parametre en post du produit.
        var newProduit = models.Produit.build({
            nom:            req.body.nom,
            description:    req.body.description,
            image:          req.body.image,
            prix :          req.body.prix,
            user_id:        1,
            categorie_id:   req.body.categorie
        });

        // On réalise une transaction vers la db.
        models.sequelize.transaction(function(t){
            // Si l'entité n'existe pas on l'ajoute.
            return models.Produit.findOrCreate({
                where: {
                    nom:            newProduit.nom,
                    description:    newProduit.description,
                    image:          newProduit.image,
                    prix :          newProduit.prix,
                    user_id:        newProduit.user_id,
                    categorie_id:   newProduit.categorie_id
                },
                transaction: t
            })
            .spread(function(produitResultat, created){ 
                // true si l'entité a été créé. False si elle existe deja
                if (created){
                    // L'entité à été ajouté.
                    res.json({status: "success", message : "Produit ajouté."});
                }
                else {
                    // L'entité existe deja dans la db.
                    res.json({status: "error", message : "Le produit existe déjà."});
                }     
            }); 
        }); 
        
     },

    // daelete
    // token require + admin 1
    delete: function (req, res) {        
        // on vérifie que l'utilisateur logé est admin
        if(req.authInfo.admin != 1)
            // l'user n'existe pas.
            res.json({status: "error", message : "L'user n'existe pas."});

        var id = parseInt(req.params.id);

        // on vérifie si le produit à supprimer existe 
        models.Produit.findById(id).then(produit => {
            if (produit == null){
                // le produit n'existe pas.
                res.json({status: "error", message : "Le produit que vous souhaitez supprimer n'existe pas"});
            }else{
                // on supprime l'entité
                produit.destroy();

                res.json({status: "success", message : "Le produit n° " + id + " a été supprimé."});
            }
        });    
          
    }
    
}