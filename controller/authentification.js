var models = require("../database/models");

module.exports = {
    
    // getAll
    loginGet: function(req, res) {  
        console.log(req.session);

        // si une erreur existe
        if(req.param('error')){
            // on l'affiche
            return res.render("login", { error: req.param('error') } );
        }
        else{
            // sinon on affiche le formulaire
            return res.render("login", { error: '' });
        }
    },

    // getAll
    loginPost: function(req, res) {  
        var email = req.body.email;
        var password = req.body.password;

        models.User.findOne({
            where: {
                email: email,
                password: password
            }
        }).then(user => {
            if( user != null){
                // session
 
                res.redirect("/index");
            }else{
                console.log("error");
                var messageError = "Couple email + mdp incorrect.";
                res.redirect("/authentification/login/?error="+messageError);
            }
        })
    },

    // getAll
    inscriptionGet: function(req, res) {        
        // si une erreur existe
        if(req.param('error')){
            // on l'affiche
            return res.render("inscription", { error: req.param('error') } );
        }
        else{
            // sinon on affiche le formulaire
            return res.render("inscription", { error: '' });
        }
    },

    // getAll
    inscriptionPost: function(req, res) {        
        // On récupère les informations de la catégories depuis les paramètres POST.
        var newUser = models.User.build({
            nom:      req.body.nom,
            prenom:   req.body.prenom,
            email:    req.body.email,
            password: req.body.password,
            actif:      1,
            role_id:    2 // itilisateur
        });

        // On réalise une transaction vers la db.
        models.sequelize.transaction(function(t){
            // Si l'entité n'existe pas on l'ajoute.
            return models.User.findOrCreate({
                where: {
                    nom:      newUser.nom,
                    prenom:   newUser.prenom,
                    email:    newUser.email,
                    password: newUser.password
                },
                transaction: t
            }).spread(function(categorieResultat, created){ 
                // true si l'entité a été créé. False si elle existe deja
                if (created){
                    // L'entité à été ajouté.
                    res.redirect('/');
                }
                else {
                    // L'entité existe deja dans la db.
                    res.redirect('/inscription', { error: "Couple identifiant déjà prit" });
                }     
            }); 
        });
    },

}