var models = require("../database/models");
var passport = require("passport");

module.exports = {
    
    // getAll
    // token require + admin 1
    getAll:  function (req, res) {        
       // on vérifie que l'utilisateur logé est admin
        if(req.authInfo.admin != 1)
            // l'user n'existe pas.
            res.json({status: "error", message : "L'user n'existe pas."});

        models.User.findAll().then(users => {
            if (users == null || users.length == 0){
                // Aucun user dans la db.
                res.json({status: "error", message : "Aucun user."});
            }else{
                // On renvoi les users
                res.json({status: "success", data : users});                
            }
        });

    },

    // getOne id
    // token require + admin 1 OR user
    getUser: function (req, res) {        
        var id = parseInt(req.params.id);

        if (!canAccess(req.authInfo.admin, id, req.authInfo.id)){
            res.json({status: "error", message : "Vous n'etes pas autorisé à supprimer ce compte"});
        }else{

            // on récupere l'user'
            models.User.findById(id).then(user => {
                if(user != null){
                    // l'user existe, on renvoi le.
                    var messageRetour = {
                        status: "success",
                        datas : user
                    }
                    res.json(messageRetour);
                }else{
                    // l'user n'existe pas.
                    res.json({status: "error", message : "L'user n'existe pas."});
                }
            });
        }
    },

    // update id
    // token require + admin 1 OR user
    update: function (req, res) {        
        var id = parseInt(req.params.id);

        if (!canAccess(req.authInfo.admin, id, req.authInfo.id)){
            res.json({status: "error", message : "Vous n'etes pas autorisé à supprimer ce compte"});
        }else{
            // on vérifie si l'user à modifier existe 
            models.User.findById(id).then(user => {
                if (user == null){
                    // L'user n'existe pas.
                    res.json({status: "error", message : "L'user que vous souhaitez modifier n'existe pas"});
                }else{
                    // on update l'entité
                    models.User.update(req.body, {
                        where: {
                            id: id
                        }
                    });

                    res.json({status: "success", message : "L'user n° " + id + " a été modifié."});
                }
            });  
        }     
    },

    // create 
    // token require + admin 1
    create: function (req, res) {        
        // on vérifie que l'utilisateur logé est admin
        if(req.authInfo.admin != 1)
            // l'user n'existe pas.
            res.json({status: "error", message : "L'user n'existe pas."});
           
        // On recupere les parametre en post du produit.
        var newUser = models.User.build({
            nom:        req.body.nom,
            prenom:     req.body.prenom,
            email:      req.body.email,
            password :  req.body.password,
            actif:      1,
            role_id:    1    
        });

        // On réalise une transaction vers la db.
        models.sequelize.transaction(function(t){
            // Si l'entité n'existe pas on l'ajoute.
            return models.User.findOrCreate({
                where: {
                    nom:         req.body.nom,
                    prenom:      req.body.prenom,
                    email:       req.body.email,
                    password :   req.body.password,
                    actif:       newUser.actif,
                    role_id:     newUser.role_id
                },
                transaction: t
            })
            .spread(function(userResultat, created){ 
                // true si l'entité a été créé. False si elle existe deja
                if (created){
                    // L'entité à été ajouté.
                    res.json({status: "success", message : "Utilisateur ajouté."});
                }
                else {
                    // L'entité existe deja dans la db.
                    res.json({status: "error", message : "L'user' existe déjà."});
                }     
            }); 
        }); 

    },

    // daelete
    // token require + admin 1 OR user
    delete: function (req, res) {   
        var id = parseInt(req.params.id);
        
        // l'utilisateur souhaite supprimer son compte
        if (!canAccess(req.authInfo.admin, id, req.authInfo.id)){
            res.json({status: "error", message : "Vous n'etes pas autorisé à supprimer ce compte"});
        }else{
            // on vérifie si l'user à supprimer existe 
            models.User.findById(id).then(user => {
                if (user == null){
                    // l'user n'existe pas.
                    res.json({status: "error", message : "L'user que vous souhaitez supprimer n'existe pas"});
                }else{
                    // on supprime l'entité
                    user.destroy();

                    res.json({status: "success", message : "L'user n° " + id + " a été supprimé."});
                }
            });   
        }
            
    }, 

    canAccess: function(role, reqId, loggedUserId){
        // si c'est un admin             
        // Ou que l'utilisateur loggé est le proprio du compte à supprimer
        if( (role == 1) || ((loggedUserId == reqId) && (role==2)) ){
            return true;
        }

        return false;
    },

        /**
    * Revocation d'un profil depuis la base de données.
    */
    // token require + admin 1
    revoquer: function (req, res) {
        // on vérifie que l'utilisateur logé est admin
        if(req.authInfo.admin != 1)
            // l'user n'existe pas.
            res.json({status: "error", message : "L'user n'existe pas."});
            
        var id = parseInt(req.params.id);

        // On vérifie que l'utilisateur existe
        models.User.findById(id).then(user => {
            if(user!=null){
                // l'utilisateur existe, on modifie l'attribut actif à 0
                models.User.update({ actif:0 }, {where: {id:id}})
                .then(result => {
                    res.json({status: "success", message : "L'utilisateur a été révoqué."});
                });                
            }else{
                // l'utilisateur n'existe pas
                res.json({status: "success", message: "L'utilisateur n'existe pas"});       
            }
        });
    },
}