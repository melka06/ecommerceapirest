var express 			= require('express');
var bodyParser 			= require('body-parser');
var application 		= express();
var config 				= require("./misc/config"); // fichier de configuration de l'app
var enRouten 			= require('express-enrouten'); 
var models 				= require('./database/models');
var JwtBearerStrategy 	= require('passport-http-jwt-bearer');
var passport 			= require('passport');
var auth 				= require("./misc/auth");


// config
application.set('view engine', 'ejs');
application.set('views', __dirname + '/views');

// Middleware
application.use(bodyParser.urlencoded({ extended: false }));

application.use(enRouten({ 
	directory: 'routes',
}));

application.use(express.static(__dirname + '/public')); // Indique que le dossier /public contient des fichiers statiques (middleware chargé de base)


// création de la structure de la base de données
/*models.Role.sync({force:true})
	.then(function(){
		return models.User.sync({force:true});
	})
	.then(function(){
		return models.Categorie.sync({force:true});
	})
	.then(function(){
		return models.Produit.sync({force:true});
	})
	.then(function(){
		models.Role.create({
			nom: "Administrateur",
			valeur: "ADMIN"
		});
	})
	.then(function(){
		models.Role.create({
			nom: "Utilisateur",
			valeur: "USER"
		});
	})
	.then(function(){
		models.User.create({
			nom: "El Karmoudi",
		    prenom: "Mohamed",
		    email: "karmoudimohamed@gmail.com",
		    password: "toz",
		    actif: 1,
		    role_id: 1
		});
	})
	.then(function(){
		models.User.create({
			nom: "test",
		    prenom: "test",
		    email: "test@gmail.com",
		    password: "test",
		    actif: 1,
		    role_id: 2
		});
	})
	.then(function(){
		models.Categorie.create({
			nom: "Voiture",
		    description: "azeoruazoiru"
		});
	});*/

application.listen(config.port);