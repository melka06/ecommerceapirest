'use strict';
module.exports = (sequelize, DataTypes) => {
  var User = sequelize.define('User', {
    nom: DataTypes.STRING,
    prenom: DataTypes.STRING,
    email: DataTypes.STRING,
    password: DataTypes.STRING,
    actif: DataTypes.INTEGER
  }, {underscored: true});

  return User;
};