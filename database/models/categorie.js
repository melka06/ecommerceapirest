'use strict';
module.exports = (sequelize, DataTypes) => {
  var Categorie = sequelize.define('Categorie', {
    nom: DataTypes.STRING,
    description: DataTypes.TEXT
  }, {underscored: true});
  
  return Categorie;
};