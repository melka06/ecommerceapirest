var models = require("../../database/models");
var produitCtrl = require("../../controller/produits");

module.exports = function (router) {
    
    // getAll
    router.get('/', produitCtrl.getAll);

    // getOne id
    router.get('/:id', produitCtrl.getProduit);

    // update id
    // token require + admin 1
    router.patch('/:id', produitCtrl.update);

    // create
    // token require + admin 1 
    router.post('/', produitCtrl.create);

    // daelete
    // token require + admin 1
    router.delete('/:id', produitCtrl.delete);    
    
}