var models = require("../../database/models");
var categorieCtrl = require("../../controller/categories");

module.exports = function (router) {
    
    // getAll
    router.get('/', categorieCtrl.getAll);

    // getOne id
    router.get('/:id', categorieCtrl.getCategorie);

    // update id 
    // token require + admin 1
    router.patch('/:id', categorieCtrl.update);

    // create 
    // token require + admin 1
    router.post('/', categorieCtrl.create);

    // daelete
    // token require + admin 1
    router.delete('/:id', categorieCtrl.delete);    
}