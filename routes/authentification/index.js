var models = require("../../database/models");
var authentificationCtrl = require("../../controller/authentification");

module.exports = function (router) {
    
    // LOGIN 
    router.get('/login', authentificationCtrl.loginGet); 

    router.post('/login', authentificationCtrl.loginPost); 

    // inscription 
    router.get('/inscription', authentificationCtrl.inscriptionGet); 

    router.post('/inscription', authentificationCtrl.inscriptionPost); 
}