var models = require("../../database/models");
var userCtrl = require("../../controller/users");

module.exports = function (router) {
    
    // getAll
    // token require + admin 1
    router.get('/', userCtrl.getAll);

    // getOne id
    // token require + admin 1 OR user
    router.get('/:id', userCtrl.getUser);

    // update id
    // token require + admin 1 OR user
    router.patch('/:id', userCtrl.update);

    // create 
    // token require + admin 1
    router.post('/', userCtrl.create); 

    // daelete
    // token require + admin 1 OR user
    router.delete('/:id', userCtrl.delete);   

    // revoquer
    router.post('/:id', userCtrl.revoquer);   
}