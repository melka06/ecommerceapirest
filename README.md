#### Node JS Ecommerce API

Utilisateur.js
	-> POST /login			// Login d'un utilisateur
	-> POST /register		// Inscription d'un utilisateur
	-> GET	/profil/:id 	// Affiche le profil d'un utilisateur

Produit.js
	-> GET 		/produit/		// Affiche tout les produits
	-> GET 		/produit/:id 	// Affiche un produit X
	-> POST 	/produit/		// Ajoute un produit
	-> DELETE 	/produit/:id 	// Supprime un produit X

